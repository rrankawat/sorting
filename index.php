<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Sorting Unicode Sequence</title>
</head>
<body>
	<!-- User Input form -->
	<div>
		<h3>Enter the sequence you want to sort</h3>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
			Enter squence: <input type="text" name="input"><br /><br />
			<input type="submit" value="Submit" id="button">
		</form>
	</div><br />

	<!-- Output sequence -->
	<div>
		<span><strong>Output : </strong></span>
		<?php

		if($_SERVER['REQUEST_METHOD'] == 'POST') {
			$string = $_POST['input'];
			array_sorting($string);
		}

		function array_sorting($string) {

			// Split string squence into array elements (words)
			$string = explode(" ", $string);

			foreach($string as $str) {

				// Converting words into array elements
				$results = array();
				preg_match_all('/./u', $str, $results);

				foreach ( $results as $array ) {
				    
				    // Sorting array elements
				    sort($array);

				    // Concat elements back to words
				    foreach($array as $val){
				    	echo $val;
				    }
				}
				echo " ";
			}
		}

		/*
		array_sorting('lemon orange banana apple');
		array_sorting('лимон апельсин банан яблоко');
		array_sorting('αβγαβγ αβγαβγαβγ');
		*/
		?>
		</div>
</body>
</html>