# sorting-task

To write a function (or class) that accepts utf8-text and returns text in which the letters of the words are sorted alphabetically. 

Example:
'lemon orange banana apple' ---> 'elmno aegnor aaabnn aelpp' 
'лимон апельсин банан яблоко' ---> 'илмно аеилнпсь аабнн бклооя' 
'αβγαβγ αβγαβγαβγ' ---> 'ααββγγ αααβββγγγ'


Do the same with additional requirements:
1. format the result as a finished project on github 
2. use composer to autoload classes and dependencies 
3. cover unit-test code 
